class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        if self.raiz is None:
            self.raiz = Nodo(elemento)
        else:
            self._insertar_recursivo(self.raiz, elemento)

    def _insertar_recursivo(self, nodo_actual, elemento):
        if elemento < nodo_actual.valor:
            if nodo_actual.izquierda is None:
                nodo_actual.izquierda = Nodo(elemento)
            else:
                self._insertar_recursivo(nodo_actual.izquierda, elemento)
        elif elemento > nodo_actual.valor:
            if nodo_actual.derecha is None:
                nodo_actual.derecha = Nodo(elemento)
            else:
                self._insertar_recursivo(nodo_actual.derecha, elemento)

    def eliminar(self, elemento):
        self.raiz = self._eliminar_recursivo(self.raiz, elemento)

    def _eliminar_recursivo(self, nodo_actual, elemento):
        if nodo_actual is None:
            return nodo_actual

        if elemento < nodo_actual.valor:
            nodo_actual.izquierda = self._eliminar_recursivo(nodo_actual.izquierda, elemento)
        elif elemento > nodo_actual.valor:
            nodo_actual.derecha = self._eliminar_recursivo(nodo_actual.derecha, elemento)
        else:
            if nodo_actual.izquierda is None:
                temp = nodo_actual.derecha
                nodo_actual = None
                return temp
            elif nodo_actual.derecha is None:
                temp = nodo_actual.izquierda
                nodo_actual = None
                return temp

            temp = self._minimo_valor_nodo(nodo_actual.derecha)
            nodo_actual.valor = temp.valor
            nodo_actual.derecha = self._eliminar_recursivo(nodo_actual.derecha, temp.valor)

        return nodo_actual

    def _minimo_valor_nodo(self, nodo):
        actual = nodo
        while actual.izquierda is not None:
            actual = actual.izquierda
        return actual

    def existe(self, elemento):
        return self._existe_recursivo(self.raiz, elemento)

    def _existe_recursivo(self, nodo_actual, elemento):
        if nodo_actual is None:
            return False

        if elemento == nodo_actual.valor:
            return True

        if elemento < nodo_actual.valor:
            return self._existe_recursivo(nodo_actual.izquierda, elemento)
        else:
            return self._existe_recursivo(nodo_actual.derecha, elemento)

    def maximo(self):
        if self.raiz is None:
            return None

        nodo_actual = self.raiz
        while nodo_actual.derecha is not None:
            nodo_actual = nodo_actual.derecha

        return nodo_actual.valor

    def minimo(self):
        if self.raiz is None:
            return None

        nodo_actual = self.raiz
        while nodo_actual.izquierda is not None:
            nodo_actual = nodo_actual.izquierda

        return nodo_actual.valor

    def altura(self):
        return self._altura_recursiva(self.raiz)

    def _altura_recursiva(self, nodo_actual):
        if nodo_actual is None:
            return 0

        altura_izquierda = self._altura_recursiva(nodo_actual.izquierda)
        altura_derecha = self._altura_recursiva(nodo_actual.derecha)

        return 1 + max(altura_izquierda, altura_derecha)

    def obtener_elementos(self):
        elementos = []
        self._in_order(self.raiz, elementos)
        return elementos

    def _in_order(self, nodo_actual, elementos):
        if nodo_actual:
            self._in_order(nodo_actual.izquierda, elementos)
            elementos.append(nodo_actual.valor)
            self._in_order(nodo_actual.derecha, elementos)

    def __str__(self):
        elementos=self.obtener_elementos()
        return "[ " + " ".join(map(str, elementos)) + " ]"
        
